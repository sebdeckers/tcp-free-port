const { Server } = require('net')

module.exports =
function freeTcpPort (callback) {
  const server = new Server()
  server.once('listening', () => {
    const { port } = server.address()
    server.close(() => {
      server.removeAllListeners()
      if (callback) callback(null, port)
    })
  })
  server.once('error', (error) => {
    server.removeAllListeners()
    if (callback) callback(error)
  })
  server.listen(0)
}
