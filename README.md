# tcp-free-port

Get a free tcp port, for node

## Install

```
npm i tcp-free-port
```

## Usage

```js
const freeTcpPort = require('tcp-free-port')

freeTcpPort((error, port) => {
  if (error) throw error // or do something else

  // use your port!
  const server = net.createServer()
  server.listen(port)
})
```

## Related

- [udp-free-port](https://github.com/mcollina/udp-free-port)

## License

ISC

## Colophon

Made with ❤️ by Sebastiaan Deckers in 🇸🇬 Singapore.
