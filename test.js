const { test } = require('ava')
const { Server } = require('net')
const onEvent = require('p-event')
const tcpFreePort = require('.')

for (let i = 0; i < 100; i++) {
  test.cb('Get a free port ' + i, (t) => {
    tcpFreePort((error, port) => {
      t.falsy(error)
      t.is(typeof port, 'number')
      t.not(port, 0)
      t.notThrows(async () => {
        const server = new Server()
        server.listen(port)
        await onEvent(server, 'listening')
        server.close()
        await onEvent(server, 'close')
        t.end()
      })
    })
  })
}
